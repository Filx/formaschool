# FormaSchool

## Authors

- BURIE Félix
- FAHAM Bouchaïb
- NOVELLI Luca
- VENNIN Jason

## Description

Collaborative platform for training courses


## Require

- minikube

## Technologies et outils utilisés

- Angular
- Spring
- Java
- HTML
- CSS
- Git
- Docker
- Maven


## Start

```
minikube start
kubectl apply -f ./k8s-v1
minikube tunnel
```


## Build (docker-compose)

```
git init
git remote add origin https://github.com/flix-flix/FormaSchool.git
git pull origin main
git submodule update --init --recursive

docker compose up -d
```
